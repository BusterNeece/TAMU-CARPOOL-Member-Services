$(function() {
    $('.ndr-btn-status').click(function() {
        var status_type = $(this).attr('rel');
        $('input.ndr-member-option[value='+status_type+']').click();
        return false;
    });

    initSort();
    $(".cp_droppable_container").sortable({
        connectWith: '.cp_droppable_container',
        update: function(event, ui) { getSortResults(); }
    }).disableSelection();
    $("#btn-reset-assignment").click(function(e) {
        e.preventDefault();
        if (confirm('Are you sure?')) {
            resetAssignment();
        }
        return false;
    });

    $('.debriefing_detail').hide();
    $('.debriefing_label').click(function() {
        var user_id = $(this).attr('rel');
        $('#debriefing_detail_'+user_id).toggle();
        return false;
    });
    
    $('#btn_email_couches').click(function(e) {
        $.ajax({
            type: "GET",
            url: url_email_couches,
            dataType: "json",
            success: function(data) {
                alert(data.result);
            }
        });
        
        e.preventDefault();
    });

    $('input.postop-sum-field, input.postop-diff-field').keyup(function() { recalculatePostOpTotals(); });
    recalculatePostOpTotals();
    
    $('#btn_refresh_from_phoneroom').click(function(e) {
        
        $.ajax({
            type: "GET",
            url: url_phoneroom_refresh,
            dataType: "json",
            success: function(data) {
                $('#fld_report_postops_rides_done').val(data.done);
                $('#fld_report_postops_rides_missed').val(data.cpmissed);
                $('#fld_report_postops_rides_cancelled').val(data.cancelled);
                $('#fld_report_postops_rides_ng').val(data.ng);
            }
        });
        
        e.preventDefault();
    });

    $('#btn_printable').click(function(e) {
        e.preventDefault();

        if (confirm('Are you sure you want to open printable view? This view does not allow for editing the form.'))
        {
            $('ul.nav-tabs').hide();
            $('.tab-pane').addClass('active').append('<br /><hr /><br />');

            $('.buttons, .smallbuttons, .btn').hide();
            $('.notes_field').each(function() {
                var field_val = $(this).val();
                $(this).closest('dd').html(field_val);
            });
            $('.debriefing_detail').show();
        }
        return false;
    });
    
    $('form.readonly').submit(function() {
        alert('This form is currently in "Read Only" mode.');
        return false;
    });

    $('#btn_quick_save').click(function(e) {
        e.preventDefault();

        if ($('#ndr_form').hasClass('readonly'))
        {
            alert('This form is currently in "Read Only" mode.');
            return false;
        }

        $.ajax({
            type: "POST",
            url: $(this).attr('href'),
            data: {
                notes: $('#notes_notes').val()
            },
            dataType: "json",
            success: function(data) {
                $('#quicksave_status').addClass('valid').show().html(data.message);
            }
        });
    });
});

function initSort()
{
    var current_positions = $('#assignment_results').val();
    if (current_positions.length != 0)
    {
        var positions_split = current_positions.split('|');
        for (i in positions_split)
        {
            var position_raw = positions_split[i];
            var position_split = position_raw.split(':');
            
            if (position_split.length == 2)
            {
                var user_id = position_split[0];
                var container = position_split[1];
                $('#assignment_member_'+user_id).appendTo('#assignment_'+container+' .cp_droppable_container');
            }
        }
    }
}

function resetAssignment()
{
    $('.cp_draggable').each(function() {
        $(this).appendTo('#assignment_unassigned .cp_droppable_container');
    });
}

var sort_results;
function getSortResults()
{
    sort_results = '';
    $('.cp_draggable').each(function() {
        var user_id = $(this).attr('rel');
        var container = $(this).closest('.cp_droppable').attr('rel');
        sort_results += user_id+':'+container+'|';
    });
    $('#assignment_results').val(sort_results);
}

var post_op_total;
function recalculatePostOpTotals()
{
    post_op_total = 0;
    
    $('input.postop-sum-field').each(function() {
        if ($(this).val() != '')
            post_op_total += parseFloat($(this).val()) * parseInt($(this).attr('rel'));
    });
    $('input.postop-diff-field').each(function() {
        if ($(this).val() != '')
            post_op_total -= parseFloat($(this).val()) * parseInt($(this).attr('rel'));
    });
    
    $('#postop-total').html('$'+post_op_total.toFixed(2));
}