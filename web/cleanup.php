<?php
require_once dirname(__FILE__) . '/../app/bootstrap.php';
$application->bootstrap();

$em = Zend_Registry::get('em');
$users = $em->createQuery('SELECT u FROM \Entity\User u WHERE u.username != :username')
	->setParameter('username', 'bneece')
	->execute();

foreach($users as $user)
{
	$user->delete();
}

echo 'Users cleared out.';