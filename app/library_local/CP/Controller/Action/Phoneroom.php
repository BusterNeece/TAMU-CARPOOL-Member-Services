<?php
namespace CP\Controller\Action;

use \Entity\Ndr;
use \Entity\Settings;

class Phoneroom extends \DF\Controller\Action
{
    /**
     * Pre-Dispatcher to handle NDR IDs and the date chooser.
     */
    
    protected $_ndr_id;
    protected $_ndr;
    
    protected $_date_unix;
    protected $_date_mysql;
    
    public function init()
    {
        parent::init();

        /**
         * Initialize the phoneroom session.
         */
        
        $phoneroom_session = new \Zend_Session_Namespace('CP_Phoneroom');
        
        if ($this->_hasParam('ndr_id'))
        {
            $this->_ndr_id = (int)$this->_getParam('ndr_id');
        }
        else if (isset($phoneroom_session->ndr_id))
        {
            $this->_ndr_id = $phoneroom_session->ndr_id;
        }
        else if ($this->_getParam('controller') != "select")
        {
            $active_ndrs = \Entity\Ndr::fetchActive();
            
            if (count($active_ndrs) == 0)
            {
                throw new \DF\Exception\DisplayOnly('There are no currently active NDRs to attach phoneroom items to.');
            }
            else if (count($active_ndrs) == 1)
            {
                $this->_ndr_id = $active_ndrs[0]['id'];
            }
            else
            {
                $current_controller = $this->_getParam('controller');
                $this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'select', 'action' => 'index', 'current' => $current_controller));
                return;    
            }
        }
        else
        {
            return;
        }
        
        $phoneroom_session->ndr_id = $this->_ndr_id;
        
        $this->_ndr = \Entity\Ndr::fetchById($this->_ndr_id);
        $this->_date_unix = strtotime($this->_ndr->event->event_date->getTimestamp());
        $this->_date_mysql = date('Y-m-d', $this->_date_unix);
        
        $this->view->ndr = $this->_ndr;
        
        /* Menu stats */
        $this->view->type_sums = $this->_getTypeSums();
        $this->view->channel_name = $this->_getChannelName();

        /* Permissions. */
        $isAllowed = $this->_permissions();
        
        if (!$isAllowed)
        {
            if (!\DF\Auth::isLoggedIn())
                throw new \DF\Exception\NotLoggedIn();
            else
                throw new \DF\Exception\PermissionDenied();
        }
    }
    
    public function preDispatch()
    {
        parent::preDispatch();
        
        // Show current page.
        $front = $this->getFrontController();  
		$request = $this->getRequest();  
        $this->view->current_page = $request->getControllerName();
    }
    
	/**
	 * Global permission checking for all Phoneroom pages.
	 */
	public function permissions()
	{
        return true;
    }

    protected function _permissions()
    {
		if ($this->acl->isAllowed('access exec features'))
		{
			return true;
		}
		else if ($this->_ndr_id)
		{
			$user = \DF\Auth::getLoggedInUser();
			
			// View members who signed up for the night associated with this report.
            try
            {
                $is_signed_up = $this->em->createQuery('SELECT s FROM Entity\Signup s WHERE s.event_id = :event_id AND s.user_id = :user_id')
                    ->setParameter('event_id', $this->_ndr->event->num)
                    ->setParameter('user_id', $user->id)
                    ->getSingleResult();
    			
    			return ($is_signed_up instanceof \Entity\Signup);
            }
            catch(\Exception $e)
            {
                return false;
            }
		}
	}

    protected function _sendUpdate($record, $action='change')
    {
        $pubnub = new \DF\Service\PubNub;
        $pubnub->publish($this->_getChannelName(), array(
            'action' => $action,
            'record' => \CP\Phoneroom::processRide($record->toArray()),
            'type_sums' => $this->_getTypeSums(),
        ));
    }

    protected function _getTypeSums()
    {
        $type_sums = array();
        
        if ($this->_ndr)
        {
            // Send required information for the site's layout to the template engine.
            $today_rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id')
                ->setParameter('ndr_id', $this->_ndr_id)
                ->getArrayResult();
            
            foreach((array)$today_rides as $ride)
            {
                $type_sums[$ride['status']] += (int)$ride['riders'];
            }
        }

        return $type_sums;
    }

    protected function _getChannelName()
    {
        return 'tamu_carpool_updates_'.$this->_ndr_id;
    }
}