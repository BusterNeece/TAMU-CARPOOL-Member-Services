<?
/**
 * CARPOOL PhoneRoom main class.
 */
 
namespace CP;

class PhoneRoom
{
	
	// Process the raw ride data from the database.
	public static function processRide(&$ride, $key = NULL)
	{
        $timestamp_fields = array('timetaken', 'timedone', 'timeassigned');
        foreach($timestamp_fields as $ts_field)
        {
            $ride[$ts_field] = ($ride[$ts_field] instanceof \DateTime) ? $ride[$ts_field]->getTimestamp() : (int)$ride[$ts_field];
        }
        
		// Format cell phone number.
		if (!empty($ride['cell']))
		{
			$ride['cell1'] = substr($ride['cell'], 0, 3);
			$ride['cell2'] = substr($ride['cell'], 3, 3);
			$ride['cell3'] = substr($ride['cell'], 6, 4);
			$ride['cell_display'] = '('.$ride['cell1'].') '.$ride['cell2'].'-'.$ride['cell3'];
		}
		else
		{
			$ride['cell_display'] = '&nbsp;';
		}
		
		// Determine total wait time if applicable.		
		if ($ride['pickup'] == "ng")
		{
			$ride['wait_time'] = '-';
			$ride['wait_time_class'] = 'circuit';
		}
		else
		{
            if ($ride['status'] == "cpmissed" || $ride['status'] == "cancelled")
				$wait_time_seconds = $ride['timedone'] - $ride['timetaken'];
			else if ($ride['status'] == "waiting")
				$wait_time_seconds = time() - $ride['timetaken'];
			else
				$wait_time_seconds = $ride['timeassigned'] - $ride['timetaken'];
			
			$wait_time_minutes = round($wait_time_seconds / 60, 0);

			if ($wait_time_minutes < 30)
				$ride['wait_time_class'] = 'short';
			else if ($wait_time_minutes < 50)
				$ride['wait_time_class'] = 'med';
			else
				$ride['wait_time_class'] = 'long';
			
			$ride['wait_time'] = $wait_time_minutes.'m';
		}
		
		// Determine total rode time.
		if ($ride['pickup'] == "ng")
		{
			$ride['rode_time_class'] = 'circuit';
			$ride['rode_time'] = '-';
		}
		else if ($ride['status'] == "cpmissed" || $ride['status'] == "cancelled")
		{
			$ride['rode_time_class'] = '';
			$ride['rode_time'] = '-';
		}
		else
		{
			$ride_time_seconds = $ride['timedone'] - $ride['timetaken'];
			$ride_time_minutes = round($ride_time_seconds / 60, 0);
			
			if ($ride_time_minutes < 30)
				$ride['rode_time_class'] = 'short';
			else if ($ride_time_minutes < 60)
				$ride['rode_time_class'] = 'med';
			else
				$ride['rode_time_class'] = 'long';
			
			$ride['rode_time'] = $ride_time_minutes.'m';
		}
		
		// Time of completion.
		if ($ride['status'] == "done")
			$ride['home_time'] = date('h:i', $ride['timedone']);
		else
			$ride['home_time'] = '-';
		
		// Time assigned.
		$assigned_time_seconds = time() - $ride['timeassigned'];
		$assigned_time_minutes = round($assigned_time_seconds / 60, 0);
		
		if ($assigned_time_minutes < 30)
			$ride['assigned_time_class'] = 'short';
		else if ($assigned_time_minutes < 50)
			$ride['assigned_time_class'] = 'med';
		else
			$ride['assigned_time_class'] = 'long';
		
		$ride['assigned_time'] = $assigned_time_minutes.'m';

		return $ride;
	}
}