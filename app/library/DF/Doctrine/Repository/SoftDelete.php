<?php
/**
 * Doctrine/DF Cache Connector
 */

namespace DF\Doctrine\Repository;

class SoftDelete extends \Doctrine\ORM\EntityRepository
{
	public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
	{
		return parent::findBy($this->fixCriteria($criteria));
	}
	 
	public function findOneBy(array $criteria)
	{
		return parent::findOneBy($this->fixCriteria($criteria));
	}
	 
	public function find($id, $lockMode = \Doctrine\DBAL\LockMode::NONE, $lockVersion = null)
	{
		return $this->findOneBy(array(
			'id' => $id
		));
	}
	 
	protected function fixCriteria(array $criteria)
	{
		//Unless explicitly requested to return deleted items, we want to return non-deleted items by default
		if(!in_array('deleted_at', $criteria)) {
			$criteria['deleted_at'] = NULL;
		}
	 
		return $criteria;
	}
}