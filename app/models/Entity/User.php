<?php
namespace Entity;

/**
 * User
 *
 * @Table(name="user",indexes={@index(name="uin_idx", columns={"uin"})})
 * @Entity
 */
class User extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->signups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nice_things = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nice_things_submitted = new \Doctrine\Common\Collections\ArrayCollection();
        $this->points = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ndr_debriefings = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="team_id", type="integer", nullable=true) */
    protected $team_id;

    /** @Column(name="uin", type="integer", nullable=true) */
    protected $uin;

    /** @Column(name="username", type="string", length=255, nullable=true) */
    protected $username;

    /** @Column(name="password", type="string", length=255, nullable=true) */
    protected $password;

    /** @Column(name="email", type="string", length=255, nullable=true) */
    protected $email;

    /** @Column(name="firstname", type="string", length=255, nullable=true) */
    protected $firstname;

    /** @Column(name="lastname", type="string", length=255, nullable=true) */
    protected $lastname;

    /** @Column(name="mi", type="string", length=255, nullable=true) */
    protected $mi;

    /** @Column(name="class", type="string", length=255, nullable=true) */
    protected $class;

    /** @Column(name="sex", type="string", length=10, nullable=true) */
    protected $sex;

    /** @Column(name="cphone", type="string", length=255, nullable=true) */
    protected $cphone;

    /** @Column(name="lphone", type="string", length=255, nullable=true) */
    protected $lphone;

    /** @Column(name="lcity", type="string", length=255, nullable=true) */
    protected $lcity;

    /** @Column(name="lstreet", type="string", length=255, nullable=true) */
    protected $lstreet;

    /** @Column(name="lzip", type="string", length=20, nullable=true) */
    protected $lzip;

    /** @Column(name="dl", type="string", length=50, nullable=true) */
    protected $dl;

    /** @Column(name="dl_curr", type="string", length=50, nullable=true) */
    protected $dl_curr;

    /** @Column(name="dl_state", type="string", length=5, nullable=true) */
    protected $dl_state;

    /** @Column(name="dob", type="string", length=25, nullable=true) */
    protected $dob;

    /** @Column(name="emer1_name", type="string", length=255, nullable=true) */
    protected $emer1_name;

    /** @Column(name="shirt_size", type="string", length=5, nullable=true) */
    protected $shirt_size;

    /** @Column(name="emer1_phone", type="string", length=255, nullable=true) */
    protected $emer1_phone;

    /** @Column(name="emer2_name", type="string", length=255, nullable=true) */
    protected $emer2_name;

    /** @Column(name="emer2_phone", type="string", length=255, nullable=true) */
    protected $emer2_phone;

    /** @Column(name="pphone", type="string", length=255, nullable=true) */
    protected $pphone;

    /** @Column(name="pcity", type="string", length=255, nullable=true) */
    protected $pcity;

    /** @Column(name="pstreet", type="string", length=255, nullable=true) */
    protected $pstreet;

    /** @Column(name="pstate", type="string", length=255, nullable=true) */
    protected $pstate;

    /** @Column(name="medins", type="string", length=255, nullable=true) */
    protected $medins;

    /** @Column(name="pzip", type="string", length=20, nullable=true) */
    protected $pzip;

    /** @Column(name="allergies", type="text", nullable=true) */
    protected $allergies;

    /** @Column(name="food_needs", type="text", nullable=true) */
    protected $food_needs;

    /** @Column(name="semesters", type="float", length=15, nullable=true) */
    protected $semesters;

    /** @Column(name="major", type="string", length=255, nullable=true) */
    protected $major;

    /** @Column(name="pis_graduation", type="string", length=50, nullable=true) */
    protected $pis_graduation;

    /** @Column(name="pis_returning", type="string", length=50, nullable=true) */
    protected $pis_returning;

    /** @Column(name="pis_nightsandpoints", type="string", length=100, nullable=true) */
    protected $pis_nightsandpoints;

    /** @Column(name="pis_leavingreason", type="text", nullable=true) */
    protected $pis_leavingreason;

    /** @Column(name="flag_dues", type="integer", length=1, nullable=true) */
    protected $flag_dues;

    /** @Column(name="flag_trained", type="integer", length=1, nullable=true) */
    protected $flag_trained;

    /** @Column(name="flag_m1", type="integer", length=1, nullable=true) */
    protected $flag_m1;

    /** @Column(name="flag_m2", type="integer", length=1, nullable=true) */
    protected $flag_m2;

    /** @Column(name="flag_m3", type="integer", length=1, nullable=true) */
    protected $flag_m3;

    /** @Column(name="flag_m4", type="integer", length=1, nullable=true) */
    protected $flag_m4;

    /** @Column(name="flag_m5", type="integer", length=1, nullable=true) */
    protected $flag_m5;

    /** @Column(name="flag_m6", type="integer", length=1, nullable=true) */
    protected $flag_m6;

    /** @Column(name="flag_m7", type="integer", length=1, nullable=true) */
    protected $flag_m7;

    /** @Column(name="flag_m8", type="integer", length=1, nullable=true) */
    protected $flag_m8;

    /** @Column(name="sms_number", type="string", length=50, nullable=true) */
    protected $sms_number;

    /** @Column(name="sms_initials", type="string", length=5, nullable=true) */
    protected $sms_initials;
    
    /** @Column(name="deleted_at", type="datetime", nullable=true) */
    protected $deleted_at;

    /**
     * @ManyToOne(targetEntity="Team", inversedBy="members")
     * @JoinColumn(name="team_id", referencedColumnName="team_id", onDelete="CASCADE")
     */
    protected $team;

    /**
     * @ManyToMany(targetEntity="Role", inversedBy="users")
     * @JoinTable(name="user_has_role",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    protected $roles;
    
    /** @OneToMany(targetEntity="Signup", mappedBy="user") */
    protected $signups;
    
    /** @OneToMany(targetEntity="NiceThing", mappedBy="user") */
    protected $nice_things;
    
    /** @OneToMany(targetEntity="NiceThing", mappedBy="submitter") */
    protected $nice_things_submitted;
    
    /** @OneToMany(targetEntity="Point", mappedBy="user") */
    protected $points;
    
    /** @OneToMany(targetEntity="NdrDebriefing", mappedBy="user") */
    protected $ndr_debriefings;
    
    public function isActive()
    {
		if (\DF\Acl::isAllowed('access exec features'))
			return true;
		else if (Settings::getSetting('cp_check_dues_and_training', 0) == 1)
			return ($this->flag_dues == 1 && $this->flag_trained == 1);
		else
			return true;
    }

    public function getNameLastFirst()
    {
        $nameparts = array($this->lastname, $this->firstname);

        if (count(array_filter($nameparts)) == 0)
            return false;
        else
            return implode(', ', $nameparts);
    }

    public function getNameFirstLast()
    {
        $nameparts = array($this->firstname, $this->lastname);

        if (count(array_filter($nameparts)) == 0)
            return false;
        else
            return implode(' ', $nameparts);
    }
    
    public function getNightsAndPoints($reload = false)
	{
		return self::getUserNightsAndPoints($this->id, $this->uin, $reload);
	}
	
	public function getAvatar($size = 'normal')
	{
		return self::getUserAvatar($this->uin, $size);
	}
    
    /**
     * Static Functions
     */
	
	public static function fetchSelect()
    {
		$results_raw = self::fetchArray();
		$select = array();
		
		foreach((array)$results_raw as $user)
		{
			$select[$user['id']] = $user['lastname'].', '.$user['firstname'];
		}
		
		return $select;
	}
    
    /* Get or create user by UIN. */
    public static function getOrCreate($uin, $save_mode = TRUE)
    {
        $record = self::getRepository()->findOneBy(array('uin' => $uin));
        
        if (!($record instanceof self))
        {
            $record = new self();
            $record->username = $uin;
            $record->uin = $uin;
            $record->password = ' ';
            $record->assignFromDirectory();

            if ($save_mode)
                $record->save();
        }
        return $record;
    }
    
    public function assignFromDirectory()
    {
		$directory = \DF\Service\TamuRest::getByUin($this->uin);
		
		if ($directory)
		{
			// Special replacement rules for NetID.
			if (isset($directory['tamuEduPersonNetID'][0]))
                $this->username = $directory['tamuEduPersonNetID'][0];
			
			$replace_values = array(
				'firstname'		=> $directory['givenName'][0],
				'lastname'		=> $directory['sn'][0],
				'email'			=> $directory['mail'][0],
				'lphone'		=> preg_replace('/[^\d]/', '', $directory['telephoneNumber'][0]),
			);
			
			foreach($replace_values as $replace_local => $replace_value)
			{
				$replace_current = $this->{$replace_local};
				if (empty($replace_current) && !empty($replace_value))
					$this->{$replace_local} = $replace_value;
			}
		}
        
        return $this;
    }
    
    /**
     * CARPOOL-Specific Functionality
     */
	
	public static function getUserNightsAndPoints($user_id, $uin, $reload = FALSE)
	{
        $em = \Zend_Registry::get('em');
        $cache_name = 'nights_and_points_'.$user_id.'_'.$uin;

        $result = \DF\Cache::get($cache_name);
        
        if (!$result || $reload)
        {
            $result = array();
            
            // Points
            $points_raw = $em->createQuery('SELECT p FROM Entity\Point p WHERE p.user_id = :user_id')
                ->setParameter('user_id', $user_id)
                ->getArrayResult();
            
            foreach((array)$points_raw as $point)
            {
                $result['points_items'][] = $point;
                $result['points']['current'] += $point['points'];
                $result['points']['projected'] += $point['points'];
            }
            
            // Nights
            $events_raw = $em->createQuery('SELECT e FROM Entity\Event e INNER JOIN e.signups s WHERE s.user_id = :user_id ORDER BY e.event_date ASC')
                ->setParameter('user_id', $user_id)
                ->getArrayResult();
            
            $nights = array();
            
            $noshow_deduction = Settings::getSetting('cp_noshow_deduction', 10);
            $bonus_addition = Settings::getSetting('cp_bonus_addition', 6);
            
            // Process each event.
            foreach((array)$events_raw as $event)
            {
                $event['text'] = $event['title'];

                /*
                if ($event['category'] == "night")
                {
                    if (isset($event['ndr']))
                    {
                        $ndr = $event['ndr'];
                        $ndr_data = $ndr['ndr_data'];
                        
                        if ($ndr_data)
                        {
                            $user_positions = array();
                            $positions = array();
                            $positions_raw = explode('|', $ndr_data['assignment']['results']);
                            foreach($positions_raw as $position)
                            {
                                list($person_user_id, $position_name) = explode(":", $position);
                                $person_user_id = (int)$person_user_id;
                                $positions[$position_name][$person_user_id] = $person_user_id;
                                $user_positions[$person_user_id] = $position_name;
                            }
                            
                            if (isset($user_positions[$user_id]))
                            {
                                $position = $user_positions[$user_id];
                                
                                if ($position != "unassigned")
                                {
                                    $event['text'] .= '<br><small><b>'.ucfirst($position).'</b>';
                                    
                                    $others_in_position = $positions[$position];
                                    $others_text = array();
                                    unset($others_in_position[$user_id]);
                                    
                                    foreach((array)$others_in_position as $other_user_id)
                                    {
                                        $other_user = User::find($other_user_id);
                                        if ($other_user)
                                        {
                                            $others_text[] = $other_user->firstname .' '.$other_user->lastname;	
                                        }
                                    }
                                    
                                    if ($others_text)
                                    {
                                        $event['text'] .= ' with '.implode(' and ', $others_text);	
                                    }

                                    $event['text'] .= '</small>';
                                }
                            }
                        }
                    }
                }
                */
                
                $event['timestamp'] = strtotime($event['event_date']->format('m/d/Y').' 11:59 PM');
                $event_type = ($event['timestamp'] < time()) ? 'done' : 'future';
                
                $result['events']['total'][] = $event;
                $result['events']['total_num']++;
                $result['events'][$event_type][] = $event;
                $result['events'][$event_type.'_num']++;
                
                if ($event['category'] == "night")
                {
                    $result['nights']['total'][] = $event;
                    $result['nights']['total_num']++;	
                    $result['nights'][$event_type][] = $event;
                    $result['nights'][$event_type.'_num']++;
                    
                    if ($event['timestamp'] > time())
                    {
                        $nights['projected']++;
                    }
                    else if (in_array($event['status'], array(CP_SIGNUP_UNCONFIRMED, CP_SIGNUP_LATE, CP_SIGNUP_ONTIME, CP_SIGNUP_BONUSPOINTS)))
                    {
                        $nights['projected']++;
                        $nights['current']++;
                        
                        if ($event['status'] == CP_SIGNUP_BONUSPOINTS)
                        {
                            $points = $bonus_addition;
                        
                            $result['points_items'][] = array(
                                'timestamp'		=> time(),
                                'event'			=> 'Bonus Points from Event',
                                'points'		=> $points,
                            );
                            $result['points']['current'] += $points;
                            $result['points']['projected'] += $points;
                        }
                    }
                    else if ($event['status'] == CP_SIGNUP_NOSHOW)
                    {
                        $points = 0-$noshow_deduction;
                        
                        $result['points_items'][] = array(
                            'timestamp'		=> time(),
                            'event'			=> 'Deduction for No-Show',
                            'points'		=> $points,
                        );
                        $result['points']['current'] += $points;
                        $result['points']['projected'] += $points;
                    }
                }
                else
                {
                    $result['non_nights']['total'][] = $event;
                    $result['non_nights']['total_num']++;
                    $result['non_nights'][$event_type][] = $event;
                    $result['non_nights'][$event_type.'_num']++;
                }
                
                if ($event['extra_points'] != 0)
                {
                    $points = (int)$event['extra_points'];
                    
                    $result['points_items'][] = array(
                        'timestamp'		=> time(),
                        'event'			=> 'Special Event Points',
                        'points'		=> $points,
                    );
                    $result['points']['current'] += $points;
                    $result['points']['projected'] += $points;
                }
            }
            
            // Assign points for completed nights.
            $completed_nights_threshold = Settings::getSetting('cp_completed_night_threshold', 6);
            $completed_nights_points = Settings::getSetting('cp_completed_night_points', 0);
            
            if ($nights['current'] > $completed_nights_threshold)
            {
                $point_nights = $nights['current'] - $completed_nights_threshold;
                $points = ($point_nights * $completed_nights_points);
                
                $result['points_items'][] = array(
                    'timestamp'		=> time(),
                    'event' 		=> 'Points for Nights Worked',
                    'points'		=> $points,
                );
                $result['points']['current'] += $points;
            }
            
            // Assign points for projected nights.
            if ($nights['projected'] > $completed_nights_threshold)
            {
                $point_nights = $nights['projected'] - $completed_nights_threshold;
                $points = ($point_nights * $completed_nights_points);
                $result['points']['projected'] += $points;
            }
            
            // This cache item has a long lifetime, as it's a very heavy set of queries.
            \DF\Cache::save($result, $cache_name, array(), 86400); 
        }

        return $result;
	}
	
	public static function getUserAvatar($uin, $size = 'normal')
	{
		if ($size == "normal")
			$member_pic_base = md5($uin).'.jpg';
		else
			$member_pic_base = md5($uin).'_'.$size.'.jpg';
		
		$member_pic_path = DF_INCLUDE_STATIC.'/memimg/'.$member_pic_base;
		
		if (file_exists($member_pic_path))
			return '<img src="'.\DF\Url::content('memimg/'.$member_pic_base).'" alt="Member Picture" />';
		else if ($size != "normal")
			return self::getUserAvatar($uin);
		else
			return '<img src="'.\DF\Url::content('memimg/nophoto.jpg').'" alt="No Photo" />';
	}
    
}