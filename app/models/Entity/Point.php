<?php
namespace Entity;


/**
 * Points
 *
 * @Table(name="points")
 * @Entity
 */
class Point extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="num", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $num;

    /** @Column(name="user_id", type="integer", length=4) */
    protected $user_id;

    /** @Column(name="event", type="string", length=255) */
    protected $event;

    /** @Column(name="points", type="integer", length=4) */
    protected $points;

    /** @Column(name="timestamp", type="integer", length=4) */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="points")
     * @JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $user;
}