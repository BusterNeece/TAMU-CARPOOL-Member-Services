<?php
namespace Entity;


/**
 * File
 *
 * @Table()
 * @Entity
 * @HasLifecycleCallbacks
 */
class File extends \DF\Doctrine\Entity
{
	public function __construct()
    {
        $this->created_at = $this->updated_at = new \DateTime("now");
    }
    
    /** @PreUpdate */
    public function updated()
    {
        $this->updated_at = new \DateTime("now");
    }
    
    /**
     * @var integer $id
     *
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $path
     *
     * @Column(name="path", type="string", length=255)
     */
    protected $path;

    /**
     * @var string $title
     *
     * @Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var text $description
     *
     * @Column(name="description", type="text")
     */
    protected $description;

	/** @Column(name="created_at", type="datetime") */
    protected $created_at;
    
    /** @Column(name="updated_at", type="datetime") */
    protected $updated_at;
}