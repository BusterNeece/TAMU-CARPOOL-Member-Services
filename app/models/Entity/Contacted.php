<?php
namespace Entity;


/**
 * Contacted
 *
 * @Table(name="contacted")
 * @Entity
 */
class Contacted extends \DF\Doctrine\Entity
{
    /**
     * @Column(name="num", type="integer", length=4)
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $num;

    /** @Column(name="ndr_id", type="integer") */
    protected $ndr_id;

    /** @Column(name="carnum", type="integer") */
    protected $carnum;

    /** @Column(name="ridedate", type="datetime", length=25) */
    protected $ridedate;

    /** @Column(name="contacttime", type="datetime", length=25) */
    protected $contacttime;

    /** @Column(name="reason", type="string", length=50) */
    protected $reason;

    /**
     * @ManyToOne(targetEntity="Ndr")
     * @JoinColumn(name="ndr_id", referencedColumnName="ndr_id", onDelete="CASCADE")
     */
    protected $ndr;
}