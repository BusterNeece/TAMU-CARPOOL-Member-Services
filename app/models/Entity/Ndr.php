<?php
namespace Entity;


/**
 * Ndr
 *
 * @Table(name="ndr")
 * @Entity
 */
class Ndr extends \DF\Doctrine\Entity
{
    public function __construct()
    {
        $this->debriefings = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rides = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * @Column(name="ndr_id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /** @Column(name="event_id", type="integer") */
    protected $event_id;

    /** @Column(name="is_archived", type="integer", length=1) */
    protected $is_archived = 0;

    /** @Column(name="ndr_date", type="date", length=25) */
    protected $ndr_date;

    /** @Column(name="ndr_data", type="json", nullable=true) */
    protected $ndr_data;

    /**
     * @OneToOne(targetEntity="Entity\Event", inversedBy="ndr")
     * @JoinColumn(name="event_id", referencedColumnName="num", unique=true)
     */
    protected $event;
    
    /**
     * @OneToMany(targetEntity="NdrDebriefing", mappedBy="ndr")
     */
    protected $debriefings;
    
    /**
     * @OneToMany(targetEntity="Ride", mappedBy="ndr")
     */
    protected $rides;
    
    public function getPositions()
    {
        $em = \Zend_Registry::get('em');
        $ndr_data = $this->ndr_data;
        
        if ($ndr_data)
        {
            $user_positions = array();
            $positions = array();
            $positions_raw = explode('|', $ndr_data['assignment']['results']);
            
            foreach($positions_raw as $position)
            {
                list($person_user_id, $position_name) = explode(":", $position);
                
                $user_raw = $em->createQuery('SELECT u FROM Entity\User u WHERE u.id = :user_id')
                    ->setParameter('user_id', (int)$person_user_id)
                    ->getArrayResult();
                
                $user = $user_raw[0];
                
                $positions[$position_name][$person_user_id] = $user;
                $user_positions[$person_user_id] = $user;
            }
            
            return array(
                'by_position' => $positions,
                'by_user' => $user_positions,
            );
        }
    }
    
    /**
     * Static Functions
     */
    
	public static function fetchById($id)
	{
        return self::find($id);
	}
	
	public static function fetchByDate($date)
	{
       /*
        if (!($date instanceof \DateTime))
            $date = $date->format('Y-m-d');
            */
        
        return self::getRepository()->findOneBy(array('ndr_date' => $date));
	}
	
	public static function fetchArray($archived_status = NULL)
	{
        $em = \Zend_Registry::get('em');
        
        $qb = $em->createQueryBuilder()
            ->select('n, e')
            ->from(__CLASS__, 'n')
            ->leftJoin('n.event', 'e')
            ->orderBy('n.ndr_date', 'DESC');
            
        if (!is_null($archived_status))
            $qb->where('n.is_archived = :is_archived')->setParameter('is_archived', ($archived_status) ? 1 : 0);
		
		return $qb->getQuery()->getArrayResult();	
	}
    
    public static function fetchActive()
    {
        return self::fetchArray(0);
    }
}