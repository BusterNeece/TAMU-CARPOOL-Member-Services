<?php
return array(
    'default' => array(
		'home'		=> array(
			// Home page.
			'label'		=> 'Home',
			'module'	=> 'default',
			'order'		=> -1,
			'pages'		=> array(
				'profile'		=> array(
					'module' 		=> 'default',
					'controller' 	=> 'profile',
					'action'		=> 'edit',
				),
				'picture'		=> array(
					'module'		=> 'default',
					'controller'	=> 'profile',
					'action'		=> 'picture',
				),
				'profile_view'	=> array(
					'module'		=> 'default',
					'controller'	=> 'profile',
					'action'		=> 'view',
					'permission'	=> 'access member features',
				),
			),
		),
    ),
);
