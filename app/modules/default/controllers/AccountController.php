<?php
class AccountController extends \DF\Controller\Action
{
	public function indexAction()
	{
		$this->redirectHome();
		return;
	}
	
    public function loginAction()
    {
		$this->doNotRender();
		
		if (!isset($_REQUEST['ticket']))
			$this->storeReferrer('login');
		
		$auth_result = \DF\Auth::authenticate();
		
		if ($auth_result)
			$this->redirectToStoredReferrer('login');
		else
			$this->redirectToRoute(array('module' => 'default'));
		return;
    }

    public function logoutAction()
    {
		$auth = \DF\Auth::logout();
        session_unset();

        $this->redirectToRoute(array('module' => 'default'));
    }
}