<?php
use \Entity\Settings;
use \Entity\User;
use \Entity\Team;

class IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('is logged in');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$user = \DF\Auth::getLoggedInUser();
    
		if ($user instanceof User)
		{
			// Member picture.
			$this->view->member_pic = $user->getAvatar();
			
			if (\DF\Acl::isAllowed('access member features'))
			{
				$this->view->totals = $totals = $user->getNightsAndPoints();
			
				// Show global announcements.
				$this->view->announcements = Settings::getSetting('cp_announcement', '');
				
				// Get team specific information.
				$team = $user->team;
                
                if ($team instanceof Team)
                {
                    $this->view->team_num = $team->team_id;
					$this->view->team = $team->toArray();
                    
					$this->view->team_members = $team->members->toArray();
					$this->view->team_announcements = Settings::getSetting('cp_team_'.$team_num.'_announcement', '');
				}
			}
		}
	}
}