<?php
use \Entity\User;
use \Entity\Role;
use \Entity\Point;
use \Enitty\Settings;

class ProfileController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('is logged in');
	}
	
	public function preDispatch()
	{
		$this->_current_user = \DF\Auth::getLoggedInUser();
		
		// View any user's basic profile information.
		$this->_user_id = (int)$this->_getParam('id', $this->_current_user->id);
		$this->_is_current_user = ($this->_user_id == $this->_current_user->id);
        $this->_user = User::find($this->_user_id);
		
		if (!($this->_user instanceof User))
			throw new \DF\Exception\DisplayOnly('User not found!');
		
		$this->view->assign(array(
			'profile_user_id' 	=> $this->_user_id,
			'profile_user'		=> $this->_user,
		));
	}
	
	// View any user's basic profile information.
	public function viewAction()
	{
		$this->view->fb_link = 'https://www.facebook.com/search.php?'.http_build_query(array(
			'q' => $this->_user['firstname'].' '.$this->_user['lastname']
		));

		$form_config = $this->config->forms->profile->form->toArray();

		if (!$this->acl->isAllowed('access director features'))
		{
			$general_config = $form_config['groups']['personal'];
			$form_config['groups'] = array('personal' => $general_config);
		}

		$profile = $this->_user->toArray();
		$form = new \DF\Form($form_config);
		$form->populate($profile);

		$this->view->profile = $profile;
		$this->view->form = $form;
		$this->view->member_pic = $this->_user->getAvatar('large');
		$this->view->totals = $this->_user->getNightsAndPoints();
	}
	
	public function removepointsAction()
	{
		\DF\Acl::checkPermission('access director features');
		
		$point_num = $this->_getParam('num');
		
		$query = $this->em->createQuery('DELETE FROM \Entity\Point p WHERE p.num = :point_num AND p.user_id = :user_id')
            ->setParameter('point_num', $point_num)
            ->setParameter('user_id', $this->_user_id)
            ->execute();
		
		$this->alert('Points removed!');
		$this->redirectFromHere(array('action' => 'view', 'num' => NULL));
		return;
	}
	
	public function editAction()
	{
		if (!$this->_is_current_user)
			\DF\Acl::checkPermission('access exec features');
		
		$form = new \DF\Form($this->config->forms->profile->form);
		$form->setDefaults($this->_user->toArray());
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$this->_user->fromArray($data);
			$this->_user->save();
			
			$this->alert('Profile updated!');
			$this->redirectHome();
			return;
		}
		$this->view->form = $form;
	}
	
	public function pictureAction()
	{
		if (!$this->_is_current_user)
			\DF\Acl::checkPermission('access exec features');
		
		$avatar_sizes = array(
			'normal' => array(
				'width' => 100,
				'height' => 150,
			),
			'large' => array(
				'width' => 300,
				'height' => 450,
			),
		);
		
		if ($_FILES['picture']['name'])
		{
			$new_file_base = md5($this->_user->uin);
			
			$temp_file_path = 'memimg/'.$new_file_base.'_temp.jpg';
			\DF\File::moveUploadedFile($_FILES['picture'], $temp_file_path);
			
			foreach($avatar_sizes as $size_name => $size_info)
			{
				if ($size_name == "normal")
					$final_file_path = 'memimg/'.$new_file_base.'.jpg';
				else
					$final_file_path = 'memimg/'.$new_file_base.'_'.$size_name.'.jpg';
					
				\DF\Image::resizeImage(\DF\File::getFilePath($temp_file_path), \DF\File::getFilePath($final_file_path), $size_info['width'], $size_info['height']);
			}
			
			// Delete the original temp file.
			\DF\File::deleteFile($temp_file_path);
			
			$this->alert('New avatar uploaded!', \DF\Flash::SUCCESS);
			$this->redirectHome();
			return;
		}
	
		$this->view->max_file_size = intval(ini_get('upload_max_filesize')).' MB';
		$this->render();
	}
}