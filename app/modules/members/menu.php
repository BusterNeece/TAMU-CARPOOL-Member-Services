<?php
return array(
    'default' => array(
		'members'		=> array(
			'label'		=> 'Members',
			'module'	=> 'members',
			'permission' => 'access member features',
			'order'		=> 60,
			
			'pages'		=> array(

				'members_index' => array(
					'label'			=> 'View Members',
					'module'		=> 'members',
					'controller'	=> 'index',
					'action'		=> 'index',
					'permission'	=> 'access member features',
				),
			
				'topten' => array(
					'label'			=> 'Top Ten',
					'module'		=> 'members',
					'controller' 	=> 'index',
					'action'		=> 'topten',
					'permission'	=> 'access member features',
				),
			
				'manage_members' => array(
					'label'			=> 'Manage Membership',
					'module'		=> 'members',
					'controller'	=> 'manage',
					'permission'	=> 'access exec features',
				),	
				'report_members' => array(
					'label'			=> 'Membership Report',
					'module'		=> 'members',
					'controller'	=> 'report',
					'permission'	=> 'access exec features',
				),
				
			),
		),
    ),
);