<?php
use \Entity\Team;
use \Entity\User;
use \Entity\Settings;

class Members_IndexController extends \DF\Controller\Action
{
	/**
	 * Pre-dispatch functionality.
	 */
	public function permissions()
	{
		return \DF\Acl::isAllowed('access member features');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$team_tabs = array();
		
		$team_names_raw = Team::fetchArray();
		$Team = array('all' => array());
		$team_tabs = array('all' => 'Everyone');
		
		foreach($team_names_raw as $team)
		{
			if (stristr($team['team_name'], 'test') === FALSE)
			{
				$Team[$team['team_id']] = array();
				$team_tabs[$team['team_id']] = trim(str_replace('Team ', '', $team['team_name']));
			}
		}
		
		$all_users = $this->em->createQuery('SELECT u, r FROM \Entity\User u JOIN u.roles r ORDER BY u.lastname ASC, u.firstname ASC, u.username ASC')
            ->getArrayResult();
		
		foreach($all_users as $user)
		{
			$is_member = (stristr($user['roles'][0]['name'], 'CARPOOL') !== FALSE);
			
			if ($user['sex'] == "M")
				$group = 'males';
			else if ($user['sex'] == "F")
				$group = 'females';
			
			if ($group && $is_member)
			{
				$Team['all'][$group][] = $user;
				
				$team_id = $user['team_id'];
				if (isset($Team[$team_id]))
					$Team[$team_id][$group][] = $user;
			}
		}
		
		$team_id = ($this->_hasParam('team')) ? $this->_getParam('team') : 'all';
		
		$this->view->team_id = $team_id;
		$this->view->team_tabs = $team_tabs;
		$this->view->team_name = $team_tabs[$team_id];
		$this->view->members = $Team[$team_id];
	}
	
	public function toptenAction()
	{
		if (\DF\Cache::test('members_topten'))
		{
			$this->view->topten = \DF\Cache::load('members_topten');
		}
		else
		{
            $all_users = $this->em->createQuery('SELECT u, r FROM Entity\User u LEFT JOIN u.roles r WHERE r.id IS NOT NULL AND r.name LIKE :name ORDER BY u.lastname ASC, u.firstname ASC')
                ->setParameter('name', '%Member%')
                ->getArrayResult();
			
			$users_by_id = array();
			$users_by_stat = array();
			
			foreach($all_users as $user_row)
			{
				$statistics = User::getUserNightsAndPoints($user_row['id'], $user_row['uin']);
				
				if ($user_row['sex'] == "M")
					$group = 'males';
				else if ($user_row['sex'] == "F")
					$group = 'females';
				
				if ($group)
				{
					$user_id = $user_row['id'];
					$users_by_id[$user_id] = $user_row;
					
					$users_by_stat['nights'][$group][$user_id] = $statistics['nights']['total_num'];
					$users_by_stat['points'][$group][$user_id] = $statistics['points']['current'];
				}
			}
			
			$top_ten = array();
			foreach($users_by_stat as $stat_name => $stat_genders)
			{
				foreach($stat_genders as $gender_name => $gender_users)
				{
					arsort($gender_users);
					$top_users = array_slice($gender_users, 0, 10, TRUE);
					
					foreach((array)$top_users as $user_id => $user_score)
					{
						$user_row = $users_by_id[$user_id];
						$user_row['score'] = $user_score;
						
						$top_ten[$stat_name][$gender_name][] = $user_row;
					}
				}
			}
			
			\DF\Cache::save($top_ten, 'members_topten');
			$this->view->topten = $top_ten;
		}
		
		$this->view->can_edit_awards = \DF\Acl::isAllowed('access exec features');
		$this->view->awards = Settings::getSetting('cp_awards', '');
	}
	
	public function setawardsAction()
	{
		\DF\Acl::checkPermission('access exec features');
		
		$awards = $_REQUEST['awards'];
		Settings::setSetting('cp_awards', $awards);
		
		$this->alert('Awards updated!');
		$this->redirectFromHere(array('action' => 'topten'));
	}
}