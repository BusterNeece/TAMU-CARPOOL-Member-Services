<?php
use \Entity\Role;
use \Entity\User;

class Members_ManageController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('access exec features');
	}

	public function indexAction()
	{
		if (isset($_REQUEST['users']))
		{
			foreach($_REQUEST['users'] as $user_id => $user_row)
			{
                $user = User::find($user_id);
				$user->fromArray($user_row);
				$user->save();
			}
			
			$this->alert('Changes saved!');
			$this->redirectHere();
			return;	
		}
		
        $all_users = $this->em->createQuery('SELECT u, r, t FROM \Entity\User u JOIN u.roles r LEFT JOIN u.team t ORDER BY t.team_name ASC, u.lastname ASC, u.firstname ASC')
            ->getArrayResult();
		
		$users_by_team = array();
		foreach($all_users as $user)
		{
			$user_team = (isset($user['team']['team_name'])) ? $user['team']['team_name'] : 'No Team';

			$user['group_id'] = (int)$user['roles'][0]['id'];
			$user['group_name'] = str_replace('CARPOOL ', '', $user['roles'][0]['name']);
			
			$users_by_team[$user_team][] = $user;
		}
		
		$this->view->all_users = $users_by_team;
		
		$semesters = array();
		for($i = 0; $i <= 10; $i++)
		{
			$semesters[$i] = $i;
		}
		$this->view->semesters = $semesters;
	}
}