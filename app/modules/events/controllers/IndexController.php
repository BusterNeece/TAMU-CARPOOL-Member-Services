<?php
use \Entity\Event;

class Events_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::isAllowed('access member features');
    }
    
	public function indexAction()
	{
		$show = ($this->_hasParam('show')) ? $this->_getParam('show') : date('Ym');
        
        $calendar = new \DF\Calendar($this->_getParam('month'));
		
		// Fetch a list of events.
		$timestamps = $calendar->getTimestamps();
        $records = $this->em->createQuery('SELECT e FROM \Entity\Event e WHERE e.event_date BETWEEN :start AND :end ORDER BY e.event_date ASC')
            ->setParameter('start', date('Y-m-d', $timestamps['start']))
            ->setParameter('end', date('Y-m-d', $timestamps['end']))
            ->getArrayResult();
		
		foreach($records as &$event)
		{
            $event['category_text'] = $this->config->carpool->event_types->{$event['category']};
			
			// Get signup totals.
			$members_signed_up_raw = $this->em->createQuery('SELECT s, u, r FROM Entity\Signup s JOIN s.user u LEFT JOIN u.roles r WHERE s.event_id = :event_id')
                ->setParameter('event_id', $event['num'])
                ->getArrayResult();
			
			$members_signed_up = array();
			foreach($members_signed_up_raw as $member)
			{
				if ($member['status'] != CP_SIGNUP_TAKEMYNIGHT)
				{
					if (stristr($member['user']['roles'][0]['name'], 'Member') !== FALSE)
					{
						$gender = ($member['user']['sex'] == "M") ? "M" : "F";
						$members_signed_up['members']++;
						$members_signed_up[$gender]++;
					}
					else
					{
						$members_signed_up['execs']++;
					}
				}
			}
            
			$event['signup'] = $members_signed_up;
            
			$event['start_timestamp'] = $event['event_date']->getTimestamp();
			$event['end_timestamp'] = $event['event_date']->getTimestamp();
		}
        
        // Automatically create the "more events in next/prev month" announcements.
        $current_month = date('m', $timestamps['start']);
        $current_year = date('Y', $timestamps['end']);
        $ranges = array(
            $timestamps['start'] => array(
                mktime(0, 0, 0, $current_month-1, 1, $current_year),
                mktime(0, 0, 0, $current_month, 0, $current_year),
                'prev',
            ),
            $timestamps['end'] => array(
                mktime(0, 0, 0, $current_month+1, 1, $current_year),
                mktime(0, 0, 0, $current_month+2, 0, $current_year),
                'next',
            ),
        );
        
        foreach($ranges as $alert_timestamp => $alert_ranges)
        {
            $num_events = $this->em->createQuery('SELECT COUNT(e.num) FROM Entity\Event e WHERE e.event_date BETWEEN :start AND :end')
                ->setParameter('start', date('Y-m-d', $alert_ranges[0]))
                ->setParameter('end', date('Y-m-d', $alert_ranges[1]))
                ->getSingleScalarResult();
            
            if ($num_events > 0)
            {
                $url = \DF\Url::route(array('month' => date('Ym', $alert_ranges[0])), NULL, FALSE);
                
                $text = 'More Events in <a href="'.$url.'">'.date('F', $alert_ranges[0]).'</a>';
                if ($alert_ranges[2] == "prev")
                    $text = '&laquo; '.$text;
                else
                    $text = $text.' &raquo;';
                
                $alert_event = array(
                    'category'      => 'announcement',
                    'title'         => $text,
                    'start_timestamp' => $alert_timestamp,
                    'end_timestamp' => $alert_timestamp,
                );
                $records[] = $alert_event;
            }
        }
        
		$this->view->records = $records;
		$this->view->calendar = $calendar->fetch($records);
	}
}