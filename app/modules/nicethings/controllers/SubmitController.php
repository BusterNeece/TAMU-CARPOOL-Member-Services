<?php
use \Entity\NiceThing;
use \Entity\User;
use \Entity\Settings;

class Nicethings_SubmitController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access exec features');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
    	$status = Settings::getSetting('nicethings_enabled', 1);
    	if ($status != 1)
    		throw new \DF\Exception\DisplayOnly('Nice things are not currently being collected.');

		$user = $this->auth->getLoggedInUser();
		
		if (isset($_REQUEST['NiceThing']))
		{
			// Delete existing nice things submitted by this user.
			$delete_NiceThing = $this->em->createQuery('DELETE FROM Entity\NiceThing nt WHERE nt.submitter_id = :user_id')
                ->setParameter('user_id', $user->id)
                ->execute();
			
            foreach($_REQUEST['NiceThing'] as $user_id => $message)
			{
				if (!empty($message))
				{
					$nicething = new NiceThing();
					$nicething->user = User::find($user_id);
					$nicething->submitter = $user;
					$nicething->message = $message;
					$nicething->save();
				}
			}
			
			$this->alert('Nice things saved!');
			$this->redirectHere();
		}
		
		$nice_things = array();
		if (count($user->nice_things_submitted) > 0)
		{
			foreach($user->nice_things_submitted as $thing)
			{
				$nice_things[$thing['user_id']] = $thing['message'];
			}
		}
        
        $all_users = $this->em->createQuery('SELECT u, r FROM Entity\User u JOIN u.roles r WHERE r.id IS NOT NULL AND r.name NOT LIKE :member_role AND u.id != :user_id ORDER BY u.lastname ASC, u.firstname ASC, u.username ASC')
            ->setParameter('member_role', '%Member%')
            ->setParameter('user_id', $user->id)
            ->getArrayResult();
		
		foreach($all_users as &$user_row)
		{
			if (isset($nice_things[$user_row['id']]))
				$user_row['message'] = $nice_things[$user_row['id']];
		}
		
		$this->view->users = $all_users;
	}
}