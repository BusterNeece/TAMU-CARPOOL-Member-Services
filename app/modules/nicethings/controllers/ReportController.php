<?php
class Nicethings_ReportController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('administer all');
	}
	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
        $all_users = $this->em->createQuery('SELECT u, nt, s FROM Entity\User u LEFT JOIN u.nice_things nt JOIN nt.submitter s ORDER BY u.sex ASC, u.lastname ASC, u.firstname ASC, u.username ASC')
            ->getArrayResult();
        
        $export_data = array(array('Name', 'Nice Thing'));
        
        foreach($all_users as $user)
        {
            $export_data[] = array($user['firstname'].' '.$user['lastname']);

            $nice_things = array();
            foreach($user['nice_things'] as $thing)
                $nice_things[$thing['submitter_id']] = $thing['message'];
            
            foreach($nice_things as $message)
                $export_data[] = array(' ', $message);
            
            $export_data[] = array(' ');
        }
        
        $this->doNotRender();
        \DF\Export::exportToCSV($export_data, TRUE);
        return;
	}
}