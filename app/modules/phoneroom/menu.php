<?php
return array(
    'default' => array(
		// Phoneroom.
		'phoneroom' => array(
			'label'		=> 'Phone Room',
			'module'	=> 'phoneroom',
			'order'		=> 0,
			'permission' => 'access member features',
			'pages'		=> array(
            	'incoming'		=> array(
                	'label'			=> 'Incoming',
					'module'		=> 'phoneroom',
                    'controller'	=> 'index',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
                ),
                'waiting'		=> array(
                	'label'			=> 'Waiting',
					'module'		=> 'phoneroom',
                    'controller'	=> 'waiting',
                    'action'		=> 'index',
                    'cp_phoneroom_tab' => 'waiting',
					'permission' 	=> 'access member features',
                ),
                'riding'		=> array(
                	'label'			=> 'Riding',
					'module'		=> 'phoneroom',
                    'controller'	=> 'riding',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
                ),
                'done'			=> array(
                	'label'			=> 'Done',
					'module'		=> 'phoneroom',
                    'controller'	=> 'done',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
					'pages'			=> array(
						'done_car'		=> array(
							'module'		=> 'phoneroom',
							'controller'	=> 'done',
							'action'		=> 'car',
							'permission'	=> 'access member features',
						),
					),
                ),
				'circuit'		=> array(
                	'label'			=> 'Circuit',
					'module'		=> 'phoneroom',
                    'controller'	=> 'circuit',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
                ),
				'cars'			=> array(
					'label'			=> 'Cars',
					'module'		=> 'phoneroom',
					'controller'	=> 'cars',
					'action'		=> 'index',
					'permission' 	=> 'access member features',
				),
				'stats'			=> array(
                	'label'			=> 'Stats',
					'module'		=> 'phoneroom',
                    'controller'	=> 'stats',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
                ),
                'tracking'		=> array(
                	'label'			=> 'Tracking',
                	'module'		=> 'phoneroom',
                    'controller'	=> 'tracking',
                    'action'		=> 'index',
					'permission' 	=> 'access member features',
                ),
            ),
		),
    ),
);
