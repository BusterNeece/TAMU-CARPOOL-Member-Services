<?php
use \Entity\Ride;
class Phoneroom_EditController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
	{
		$ride_num = intval($this->_getParam('num'));

		$ride = Ride::find($ride_num);

		if (isset($_REQUEST['name']))
		{
			$update_data = array(
				'name'		=> $_REQUEST['name'],
				'cell'		=> $_REQUEST['cell1'].$_REQUEST['cell2'].$_REQUEST['cell3'],
				'riders'	=> (int)$_REQUEST['riders'],
				'pickup'	=> $_REQUEST['pickup'],
				'dropoff'	=> $_REQUEST['dropoff'],
				'location'	=> $_REQUEST['location'],
				'clothes'	=> $_REQUEST['clothes'],
				'notes'		=> $_REQUEST['notes'],
				'car'		=> (int)$_REQUEST['car'],
			);
            $ride->fromArray($update_data);
			$ride->save();

			$this->_sendUpdate($ride, 'update');
			$this->alert('Ride changes saved!');
			
			if (isset($_REQUEST['referer']))
				$this->redirect($_REQUEST['referer']);
			else
				$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'waiting', 'action' => 'index', 'num' => $ride_num));
			return;
		}
		
		$ride = $ride->toArray();
		\CP\PhoneRoom::processRide($ride, NULL);
		
		$this->view->ride = $ride;
		$this->view->pgId = $_REQUEST['pg'];
	}
}