<?php
use \Entity\Ride;

class Phoneroom_CancelController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		$ride_num = (int)$this->_getParam('num');
		$ride = Ride::find($ride_num);

		if ($ride)
		{
			$time_assigned = ($ride['timetaken'] instanceof \DateTime) ? $ride['timetaken']->getTimestamp() : 0;
			$time_diff = time() - $time_assigned;
			
			$updated_status = ($time_diff > 1800) ? 'cpmissed' : 'cancelled';
			
			$ride->status = $updated_status;
			$ride->timedone = new \DateTime('NOW');
			$ride->save();

			$this->_sendUpdate($ride, 'status');
		}
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'done', 'action' => 'index', 'num' => $ride_num));
		return;
	}
}