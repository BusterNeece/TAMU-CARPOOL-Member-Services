<?php
use \Entity\Ride;

class Phoneroom_CircuitController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		if (isset($_REQUEST['car']))
		{
            $car = (int)$_REQUEST['car'];
            
            if ($car == 0)
                throw new \DF\Exception\DisplayOnly('No car specified.');
            
			$ride = new Ride();
            $ride->ndr = $this->_ndr;
			$ride->car = $car;
			$ride->name = $_REQUEST['name'];
			$ride->riders = (int)$_REQUEST['riders'];
			$ride->pickup = 'ng';
			$ride->dropoff = $_REQUEST['dropoff'];
			$ride->location = $_REQUEST['location'];
			$ride->notes = 'circuit';
			$ride->status = 'done';
			$ride->ridedate = new \DateTime('NOW');
			$ride->timetaken = new \DateTime('NOW');
			$ride->timeassigned = new \DateTime('NOW');
			$ride->timedone = new \DateTime('NOW');
			$ride->save();
			
			$ride_num = $ride->num;
			$this->_sendUpdate($ride, 'new');
            
            $this->alert('Ride posted!');
            $this->redirectHere();
			return;
		}
	}
}