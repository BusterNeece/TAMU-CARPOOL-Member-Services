<?php
use \Entity\Ride;

class Phoneroom_DoneController extends \CP\Controller\Action\Phoneroom
{	
	/**
	 * Main display.
	 */
    public function indexAction()
    {
        // Pull rides in database with this status.
        $rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.status IN (:status) ORDER BY r.timetaken ASC')
            ->setParameters(array('ndr_id' => $this->_ndr_id, 'status' => $this->config->carpool->done_statuses->toArray()))
            ->getArrayResult();
		
		array_walk($rides, array('\CP\PhoneRoom', 'processRide'));
		$this->view->rides = $rides;
	}
	
	public function undoAction()
	{
		$ride_num = intval($this->_getParam('num'));
		$ride = Ride::find($ride_num);
		
		if ($ride)
		{
			$ride->status = ($ride['car'] == 0) ? 'waiting' : 'riding';
			$ride->save();

			$this->_sendUpdate($ride, 'status');
			
			$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => $new_status, 'action' => 'index', 'num' => $ride_num));
		}
		else
		{
			$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'done', 'action' => 'index'));
		}
		return;
	}
	
	// Pull all rides associated with a certain car.
	public function carAction()
	{
		$car_num = intval($this->_getParam('car'));

		if (isset($_REQUEST['rides']))
		{
			foreach($_REQUEST['rides'] as $ride_num => $ride_status)
			{
				$ride = Ride::find($ride_num);
				$ride->is_confirmed = $ride_status;
				$ride->save();
			}
			
			$this->alert('Confirmation status updated!');
			$this->redirectHere();
			return;
		}
        
        $rides = $this->em->createQuery('SELECT r FROM Entity\Ride r WHERE r.ndr_id = :ndr_id AND r.car = :car AND r.status IN (:status) ORDER BY r.timetaken ASC')
            ->setParameters(array(
                'ndr_id'        => $this->_ndr_id,
                'car'           => $car_num,
                'status'        => $this->config->carpool->done_statuses->toArray()
            ))->getArrayResult();
        
		array_walk($rides, array('\CP\PhoneRoom', 'processRide'));
		
		$this->view->rides = $rides;
		$this->view->car_num = $car_num;
	}
	
	// Confirm a ride when reviewing a car's travel log.
	public function confirmAction()
	{
		$car_num = intval($_REQUEST['car']);
		$ride_num = intval($_REQUEST['num']);
		$new_status = intval($_REQUEST['status']);
		
		$ride = Ride::find($ride_num);
		$ride->is_confirmed = $new_status;
		$ride->save();
		
		$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'done', 'action' => 'car', 'car' => $car_num));
		return;
	}
}