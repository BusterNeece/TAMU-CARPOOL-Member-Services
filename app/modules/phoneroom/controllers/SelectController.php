<?php
use \Entity\Ndr;

class Phoneroom_SelectController extends \CP\Controller\Action\Phoneroom
{
	/**
	 * Main display.
	 */
    public function indexAction()
    {
        if ($this->_hasParam('ndr_id'))
        {
            $dest_controller = ($this->_hasParam('current')) ? $this->_getParam('current') : 'index';
            $this->redirectToRoute(array(
                'module' => 'phoneroom',
                'controller' => $dest_controller,
                'action' => 'index',
            ));
            return;
        }
        
        $active_ndrs = Ndr::fetchActive();
        
        foreach($active_ndrs as &$ndr)
        {
            $ndr['name'] = $ndr['event']['title'];
            
            if ($ndr['is_archived'] == 1)
                $ndr['name'] .= ' (Archived)';
        }
        
        $this->view->active_ndrs = $active_ndrs;
	}
}