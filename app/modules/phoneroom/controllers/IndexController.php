<?php
use \Entity\Ride;

class Phoneroom_IndexController extends \CP\Controller\Action\Phoneroom
{
	/**
	 * Main display.
	 */
    public function indexAction()
    {
		if (isset($_REQUEST['name']))
		{
			$ride = new Ride();
            $ride->ndr = $this->_ndr;
			$ride->name = $_REQUEST['name'];
			$ride->cell = $_REQUEST['cell1'].$_REQUEST['cell2'].$_REQUEST['cell3'];
			$ride->riders = (int)$_REQUEST['riders'];
			$ride->pickup = $_REQUEST['pickup'];
			$ride->dropoff = $_REQUEST['dropoff'];
			$ride->location = $_REQUEST['location'];
			$ride->clothes = $_REQUEST['clothes'];
			$ride->notes = $_REQUEST['notes'];
			$ride->status = 'waiting';
			$ride->ridedate = new \DateTime('NOW');
			$ride->timetaken = new \DateTime('NOW');
			$ride->save();

			$this->_sendUpdate($ride, 'status');
			
			$ride_num = $ride->num;
			
			$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'waiting', 'action' => 'index', 'num' => $ride_num));
			return;
		}
	}
}