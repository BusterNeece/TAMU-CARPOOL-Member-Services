<?php
use \Entity\File;

class Files_IndexController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access member features');
	}
	
	public function indexAction()
	{
        $files = $this->em->createQuery('SELECT f FROM Entity\File f ORDER BY f.title ASC')->getArrayResult();
		
		foreach($files as &$file)
		{
			$file['url'] = \DF\Url::content($file['path']);
		}
		
		$this->view->files = $files;
	}
	
	public function editAction()
	{
		\DF\Acl::checkPermission('access exec features');
		
		$file_id = (int)$this->_getParam('id');
		$record = File::find($file_id);
		
		if (!($record instanceof File))
			throw new \DF\Exception\DisplayOnly('File not found!');
		
		$form = new \DF\Form($this->config->forms->file->form);
		$form->setDefaults($record->toArray());
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			$record->fromArray($data);
			$record->save();
			
			$this->alert('File updated!');
			$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
			return;
		}
		
		$this->view->record = $record;
		$this->view->file_name = basename($record->path);
		$this->view->form = $form;
	}
	
	public function deleteAction()
	{
		\DF\Acl::checkPermission('access exec features');
		
		$file_id = (int)$this->_getParam('id');
		$record = File::find($file_id);
		
		if (!($record instanceof File))
			throw new \DF\Exception\DisplayOnly('File not found!');
		
		$file_path = $record->path;
		\DF\File::deleteFile($file_path);
		
		$record->delete();
		
		$this->alert('File removed.');
		$this->redirectFromHere(array('action' => 'index', 'id' => NULL));
	}
	
	public function addAction()
	{
		\DF\Acl::checkPermission('access exec features');
		
		if ($_FILES['newfile']['name'])
		{
			$new_file_path = 'files/'.preg_replace("/[^a-zA-Z0-9\.]/", "", $_FILES['newfile']['name']);
			
			if (file_exists(\DF\File::getFilePath($new_file_path)))
			{
				$i = 0;
				$file_assigned = FALSE;
				$file_extension = \DF\File::getFileExtension($new_file_path);
				
				while($file_assigned == FALSE)
				{
					$attempt_file_path = str_replace('.'.$file_extension, '_'.$i.'.'.$file_extension, $new_file_path);
					if (!file_exists(\DF\File::getFilePath($attempt_file_path)))
					{
						$new_file_path = $attempt_file_path;
						$file_assigned = TRUE;
					}
					
					$i++;
				}
			}
			
			\DF\File::moveUploadedFile($_FILES['newfile'], $new_file_path);
			
			$record = new File();
			$record->path = $new_file_path;
			$record->title = basename($new_file_path);
			$record->save();
			
			$this->alert('File uploaded! Specify the file\'s details below.', \DF\Flash::SUCCESS);
			$this->redirectFromHere(array('action' => 'edit', 'id' => $record->id));
			return;
		}
		else
		{
			$this->redirectToRoute(array('action' => 'index'));
		}
	}
}