<?php
class Admin_IndexController extends \DF\Controller\Action
{
    public function permissions()
    {
        return \DF\Acl::getInstance()->isAllowed('administer all');
    }
	
    /**
     * Main display.
     */
    public function indexAction()
    { }
}