<?php
use \Entity\User;
use \Entity\Role;

class Admin_UsersController extends \DF\Controller\Action
{
    public function permissions()
    {
		return $this->acl->isAllowed('administer all');
    }

    public function indexAction()
    {
        if ($this->_hasParam('q'))
        {
            $q = $this->_getParam('q');
            $this->view->q = $q;
            
            $user_query = $this->em->createQueryBuilder()
                ->select('u, r')
                ->from('Entity\User', 'u')
                ->leftJoin('u.roles', 'r')
                ->where('u.uin LIKE :q OR u.username LIKE :q')
                ->setParameter('q', '%'.$q.'%');
        }
        else
        {
            $user_query = $this->em->createQueryBuilder()
                ->select('u, r')
                ->from('Entity\User', 'u')
                ->leftJoin('u.roles', 'r');
        }
        
        $page = $this->_getParam('page', 1);
        $pager = new \DF\Paginator\Doctrine($user_query, $page);
        $this->view->pager = $pager;
    }

    public function addAction()
    {
		$form = new \DF\Form($this->config->forms->user_new->form);
        
        if( !empty($_POST) && $form->isValid($_POST) )
        {
			$data = $form->getValues();

			$uins_raw = explode("\n", $data['uin']);
            $uins = array();
			foreach((array)$uins_raw as $uin)
			{
				$uin = trim($uin);
                if ($uin && strlen($uin) == 9)
                    $uins[$uin] = $uin;
            }

            foreach($uins as $uin)
            {
                $user = User::getOrCreate($uin, FALSE);
                $user->fromArray(array('roles' => $data['roles']));
                $this->em->persist($user);
                
                $this->alert('User '.$user->lastname.', '.$user->firstname.' successfully updated/added.', 'green');
			}

            $this->em->flush();
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
            return;
        }

        $this->view->headTitle('Add User(s)');
        $this->renderForm($form);
        return;
    }
    
    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		$form = new \DF\Form($this->config->forms->user_edit->form);
		
		if ($user instanceof User)
			$form->setDefaults($user->toArray(TRUe, TRUE));
        
		if( !empty($_POST) && $form->isValid($_POST) )
		{
			try
			{
				$data = $form->getValues();
				$user->fromArray($data);
				$user->save();
				
				$this->alert('User updated!', 'green');
			}
			catch(\Exception $e )
			{
				$form->setErrors(array(
					$e->getMessage()
				));
				$this->alert($e->getMessage(), 'red');
			}
			
			$this->redirectToRoute(array('module'=>'admin','controller'=>'users'));
            return;
		}

        $this->view->headTitle('Edit User');
        $this->renderForm($form);
    }
    
    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $user = User::find($id);
		
		if ($user instanceof User)
            $user->delete();
        
        $this->alert('<b>User removed.</b>');
        $this->redirectFromHere(array('action' => 'index', 'id' => NULL));
        return;
    }
}