<?php
return array(
    'default' => array(
        'admin' => array(
            'label' => 'Configuration',
            'module' => 'admin',
            'show_only_with_subpages' => TRUE,
			
            'order' => 100,
			
            'pages' => array(
				'settings'	=> array(
					'label'	=> 'Manage Settings',
					'module' => 'admin',
					'controller' => 'settings',
					'action' => 'index',
					'permission' => 'administer all',
				),
				
                'users' => array(
                    'label' => 'Manage Users',
                    'module' => 'admin',
                    'controller' => 'users',
					'action' => 'index',
					'permission' => 'manage users',
				),
				'permissions' => array(
					'label' => 'Permissions',
					'module' => 'admin',
					'controller' => 'permissions',
					'permission' => 'manage permissions',
					'pages' => array(),
				), 
				
				'nicethings'	=> array(
					'label'			=> 'Nice Things Report',
					'module'		=> 'nicethings',
					'controller'	=> 'report',
					'permission'	=> 'administer all',
				),
            ),
        ),
    ),
);