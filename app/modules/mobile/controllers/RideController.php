<?php
class Mobile_RideController extends \CP\Controller\Action\Mobile
{
    public function permissions()
    {
		return true;
    }
	
    /**
     * Main display.
     */
    public function indexAction()
	{
        if (isset($_REQUEST['form']))
		{
            $form_values = '';
            foreach($_REQUEST['form'] as $field_name => $field_value)
            {
                $form_values .= '<b>'.$field_name.':</b><br>';
                $form_values .= nl2br($field_value).'<br><br>';
            }
            
            \DF\Messenger::send(array(
                'to'        => 'info@carpool.tamu.edu',
                'subject'   => 'Ride Request',
                'body'      => $form_values,
            ));
            
            $this->alert('Your ride request has been submitted!');
			
			$this->redirectToRoute(array('module' => 'phoneroom', 'controller' => 'waiting', 'action' => 'index', 'num' => $ride_num));
			return;
		}
    }
}