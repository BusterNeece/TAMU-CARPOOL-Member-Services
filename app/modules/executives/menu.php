<?php
return array(
    'default' => array(
		// Executives
		'executives' => array(
			'label'		=> 'Execs',
			'module'	=> 'executives',
			'order'		=> 80,
			'permission' => 'access exec features',
			'pages'		=> array(
				'team_settings'	=> array(
					'label'			=> 'Change Team Settings',
					'module'		=> 'executives',
					'controller'	=> 'team',
					'action'		=> 'settings',
					'permission' 	=> 'access exec features',
				),
				'points'	=> array(
					'label'			=> 'Assign Points',
					'module'		=> 'executives',
					'controller'	=> 'points',
					'action'		=> 'index',
					'permission' 	=> 'access exec features',
				),
				'debriefing'	=> array(
					'label'			=> 'Submit Debriefing',
					'module'		=> 'executives',
					'controller'	=> 'debriefing',
					'action'		=> 'index',
					'permission' 	=> 'access exec features',
				),
				'nice_things_submit'	=> array(
					'label'			=> 'Submit Nice Things',
					'module'		=> 'nicethings',
					'controller'	=> 'submit',
					'action'		=> 'index',
					'permission'	=> 'access exec features',
				),
			),
		),
    ),
);
