<?php
use \Entity\Settings;

class Executives_TeamController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('access exec features');
	}

	public function preDispatch()
	{
		parent::preDispatch();
		
		// Check for team membership
		$user = \DF\Auth::getInstance()->getLoggedInUser();
		$team_num = $user->team_id;
		
		if ($team_num == 0)
			throw new \DF\Exception\DisplayOnly('You are not currently in a team!');
		
		$this->team_num = $this->view->team_num = $team_num;
	}
	
	public function indexAction()
	{
		$this->redirectToRoute(array('module' => 'executives', 'controller' => 'index', 'action' => 'index'));
	}
		
	/**
	 * Set team announcement and change other settings.
	 */
	public function settingsAction()
	{
		if (isset($_REQUEST['settings']))
		{
			foreach($_REQUEST['settings'] as $setting_name => $setting_value)
			{
				Settings::setSetting($setting_name, $setting_value);
			}
		
			$this->alert('Settings saved!');
			$this->redirectHere();
			return;
		}
		
		$this->view->site_settings = Settings::fetchAll();
	}
}