<?php
use \Entity\Team;
use \Entity\Point;
use \Entity\User;

class Executives_PointsController extends \DF\Controller\Action
{	
	public function permissions()
	{
		return \DF\Acl::getInstance()->isAllowed('access exec features');
	}

	/**
	 * Assign points to team members
	 */
	public function indexAction()
	{
		if (isset($_REQUEST['members']))
		{
			// Prevent duplicates using an intermediate array.
			$members_to_assign = array();
			foreach($_REQUEST['members'] as $member)
			{
				$members_to_assign[$member] = $member;
			}

			// Assign points and reload caches.
			$members_to_reload = array();

			foreach($members_to_assign as $member)
			{
				$member_obj = User::find($member);
				$members_to_reload[] = $member_obj;

				$point = new Point();
				$point->user = $member_obj;
				$point->event = $_REQUEST['event'];
				$point->points = (int)$_REQUEST['points'];
				$point->timestamp = time();
				$this->em->persist($point);
			}

			$this->em->flush();

			// Reload all caches.
			foreach($members_to_reload as $member)
			{
				$points = $member->getNightsAndPoints(TRUE);
			}
			
			$this->alert('<b>Points submitted!</b>', 'green');
			$this->redirectHere();
			return;
		}
		
		$team_names_raw = Team::fetchArray();
		$teams = array(
			'all'		=> array(
				'name'		=> 'Everyone',
				'users'		=> array(),
			),
		);

		foreach($team_names_raw as $team)
		{
			if (stristr($team['team_name'], 'test') === FALSE)
			{
				$teams[$team['team_id']] = array(
					'name'		=> trim(str_replace('Team', '', $team['team_name'])),
					'users'		=> array(),
				);
			}
		}
		
		$all_users = User::fetchArray();
		
		foreach($all_users as &$user)
		{
			$user_key = $user['id'];
			$user_val = $user['lastname'].', '.$user['firstname'];
			
			$teams['all']['users'][$user_key] = $user_val;
			$team_id = $user['team_id'];
			
			if (isset($teams[$team_id]))
				$teams[$team_id]['users'][$user_key] = $user_val;
		}
		
		$this->view->teams = $teams;
	}
}