<?php
use \Entity\Team;

class Directors_TeamsController extends \DF\Controller\Action
{
	public function indexAction()
	{
		$this->view->Team = Team::fetchArray();
	}
	
	public function membersAction()
	{
		$team_id = intval($this->_getParam('id'));
		
		$team = Team::find($team_id);
		if (!$team)
			throw new Exception('Team not found!');
		
		$team_info = $team->toArray();
		$this->view->team = $team_info;
		
		if (isset($_REQUEST['submitted']))
		{
			// Remove all existing members.
			if (count($team->members) > 0)
			{
				foreach($team->members as $member)
				{
					$member->team = NULL;
					$this->em->persist($member);
				}
			}
			
			// Set new members.
            $new_members = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.id IN (:user_ids)')
                ->setParameter('user_ids', $_REQUEST['members'])
                ->getResult();
			
			foreach($new_members as $member)
			{
				$member->team = $team;
				$this->em->persist($member);
			}
			
			$this->em->flush();
			$this->alert('Team members set!');
			$this->redirectFromHere(array('updated' => time()));
			return;
		}
        
        $all_users = $this->em->createQuery('SELECT u, r FROM \Entity\User u JOIN u.roles r ORDER BY u.lastname ASC, u.firstname ASC')
            ->getArrayResult();
		
		$user_options = array();
		$team_members = array();
		
		foreach($all_users as $user_row)
		{
			$user_team = $user_row['team_id'];
			if (($user_team && $user_team == $team_id) || !$user_team)
			{
				$user_options[$user_row['id']] = $user_row['lastname'].', '.$user_row['firstname'].' ('.$user_row['roles'][0]['name'].')';
			}
			
			if ($user_team == $team_id)
				$team_members[] = $user_row['id'];
		}
		
		$this->view->user_options = $user_options;
		$this->view->team_members = $team_members;
	}
	
	public function editAction()
	{
		$team_id = intval($this->_getParam('id'));
		if ($team_id != 0)
		{
			$team = Team::find($team_id);
			$this->view->team = $team->toArray();
		}
		
		if (isset($_REQUEST['team']))
		{
			$team_row = $_REQUEST['team'];
			
			if (!$team)
				$team = new Team();
			
			$team->fromArray($team_row);
			$team->save();
			
			$this->alert('Team changes saved!');
			
			$this->redirectToRoute(array('module' => 'directors', 'controller' => 'teams', 'action' => 'index'));
		}
	}
	
	public function deleteAction()
	{
		$team_id = intval($this->_getParam('id'));
		$team = Team::find($team_id);
		
		if (count($team->members) > 0)
		{
			foreach($team->members as $member)
			{
                unset($member->team);
				$member->save();	
			}
		}
		$team->delete();
		
		$this->alert('Team deleted!');
		$this->redirectToRoute(array('module' => 'directors', 'controller' => 'teams', 'action' => 'index'));
	}
}