<?php
use \Entity\User;
use \Entity\Role;
use \Entity\Action;

class Directors_ResetController extends \DF\Controller\Action
{
	public function permissions()
	{
		return $this->acl->isAllowed('access director features');
	}

    public function indexAction()
    {}

	public function cleareventsAction()
	{
		// Delete all event signups.
		$this->em->createQuery('DELETE FROM Entity\Signup s')->execute();

		// Delete all phoneroom rides.
		$this->em->createQuery('DELETE FROM Entity\Ride r')->execute();

		// Delete all phoneroom contacts.
		$this->em->createQuery('DELETE FROM Entity\Contacted c')->execute();

		$this->alert('<b>Signups and phoneroom cleared.</b>', 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}

	public function clearflagsAction()
	{
		// Delete all points
		$this->em->createQuery('DELETE FROM Entity\Point p')->execute();

		// Reset flags on all users.
		$all_members = $this->em->createQuery('SELECT u, r FROM Entity\User u LEFT JOIN u.roles r')
			->getResult();
		
		foreach($all_members as $member)
		{
			$member->fromArray(array(
				'flag_dues'		=> 0,
				'flag_trained'	=> 0,
				'flag_m1'		=> 0,
				'flag_m2'		=> 0,
				'flag_m3'		=> 0,
				'flag_m4'		=> 0,
				'flag_m5'		=> 0,
				'flag_m6'		=> 0,
				'flag_m7'		=> 0,
				'flag_m8'		=> 0,
			));
			$this->em->persist($member);
		}
		
		$this->em->flush();

		$this->alert('<b>Points and flags cleared.', 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}

	public function clearmembersAction()
	{
		// Remove members from their role.
		$all_members = $this->em->createQuery('SELECT u, r FROM Entity\User u LEFT JOIN u.roles r WHERE r.name LIKE :role_name')
			->setParameter('role_name', '%Member%')
			->getResult();

		foreach($all_members as $member)
		{
			$member->roles->clear();
			$this->em->persist($member);
		}

		// Add new members.
		if ($this->_hasParam('members'))
		{
			$member_role = $this->em->createQuery('SELECT r FROM Entity\Role r WHERE r.name LIKE :role_name')
				->setParameter('role_name', '%Member%')
				->getSingleResult();
			
			// Filter duplicates.
			$uins_raw = explode("\n", $this->_getParam('members'));
			$uins = array();
			foreach((array)$uins_raw as $uin)
			{
				$uin = trim($uin);
				if ($uin)
					$uins[$uin] = $uin;
			}

			foreach($uins as $uin)
			{
				$user = User::getOrCreate($uin, FALSE);
				$user->roles->add($member_role);
				$this->em->persist($user);
			}
		}

		$this->em->flush();

		$this->alert('<b>Membership roster cleared.', 'green');
		$this->redirectFromHere(array('action' => 'index'));
		return;
	}
}