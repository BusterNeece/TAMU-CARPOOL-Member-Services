<?php
/**
 * Director event creation and maintenance
 */

use \Entity\Event;

class Directors_EventsController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access director features');
	}
	
	public function indexAction()
	{
		$query = $this->em->createQueryBuilder()
            ->select('e')
            ->from('Entity\Event', 'e')
            ->orderBy('e.event_date', 'DESC');
		
		$page_num = ($this->_hasParam('page')) ? (int)$this->_getParam('page') : 1;
		$paginator = new \DF\Paginator\Doctrine($query, $page_num, 25);
		
		$this->view->pager = $paginator;
	}
	
	public function editAction()
	{
		$form = new \DF\Form($this->config->forms->event->form);
		
		$event_num = (int)$this->_getParam('num');
		if ($event_num != 0)
		{
			$record = Event::find($event_num);
			$form->setDefaults($record->toArray(TRUE, TRUE));
		}
		
		if (!empty($_POST) && $form->isValid($_POST))
		{
			$data = $form->getValues();
			
			if (!($record instanceof Event))
				$record = new Event();
			
			try
			{
				$record->fromArray($data);
				$record->save();
				
				$this->alert('Event saved!', 'green');
				$this->redirectToRoute(array('module' => 'directors', 'controller' => 'events', 'action' => 'index'));
				return;
			}
			catch(\Exception $e)
			{
				$this->alert('<b>An error occurred:</b> '.$e->getMessage(), 'red');
			}
		}
		$this->view->form = $form;
	}
	
	public function toggleAction()
	{
		$event_num = (int)$this->_getParam('num');
		$record = Event::find($event_num);
		
		if ($record instanceof Event)
		{
			$record->is_open = 1 - $record->is_open;
			$record->save();
		}
		
		$this->alert('Event status toggled.');
		$this->redirectFromHere(array('action' => 'index', 'num' => NULL));
		return;
	}
	
	public function deleteAction()
	{
		$event_num = intval($this->_getParam('num'));
        
		$event = Event::find($event_num);
        $event->delete();
		
		$this->alert('Event deleted!');
		$this->redirectToRoute(array('module' => 'directors', 'controller' => 'events', 'action' => 'index'));
	}
}