<?php
class Sms_SendController extends \DF\Controller\Action
{
	public function permissions()
	{
		return \DF\Acl::isAllowed('access director features');
	}
	
	public function indexAction()
	{
		$this->redirectFromHere(array('action' => 'recipients'));
	}
	
	public function recipientsAction()
	{
        $all_users = $this->em->createQuery('SELECT u, r, t FROM \Entity\User u JOIN u.roles r JOIN u.team t ORDER BY t.team_name ASC, u.lastname ASC, u.firstname ASC')
            ->getArrayResult();
		
		$users_by_team = array();
		foreach($all_users as $user)
		{
			$user_team = (isset($user['team']['team_name'])) ? $user['team']['team_name'] : 'No Team';
			$user_id = $user['id'];
			$user_role = $user['roles'][0]['name'];
			
			$users_by_team[$user_team][$user_id] = $user['lastname'].', '.$user['firstname'].' ('.$user_role.')';
		}
		
		$this->view->all_users = $users_by_team;
	}
	
	public function sendAction()
	{
		if (!$this->_hasParam('recipients'))
			$this->redirectFromHere(array('action' => 'recipients'));
		
		$recipients = $this->_getParam('recipients');
		if (!is_array($recipients))
			$recipients = explode(',', $recipients);
        
        $users_raw = $this->em->createQuery('SELECT u FROM \Entity\User u WHERE u.id IN (:user_ids) ORDER BY u.lastname ASC, u.firstname ASC')
            ->setParameter('user_ids', $recipients)
            ->getArrayResult();
		$users = array();

		foreach($users_raw as $user)
		{
			$user['sms_number'] = preg_replace('/[^\d]/', '', $user['sms_number']);
			$user['can_send_sms'] = (!empty($user['sms_number']) && !empty($user['sms_initials']));
			$users[$user['id']] = $user;
		}
		
		if (isset($_REQUEST['message']))
		{
			$result_list = '<dl><dt>Results:</dt>';

			foreach($users as $user)
			{
				$user_name = $user['firstname'].' '.$user['lastname'];

				if ($user['can_send_sms'])
				{
					try
					{
						\DF\Service\Twilio::sms($user['sms_number'], $this->config->application->name.': '.$_REQUEST['message']);
						$result_list .= '<dd class="valid">'.$user_name.': message sent successfully.</dd>';
					}
					catch(\Exception $e)
					{
						$result_list .= '<dd class="warning">'.$user_name.': error ('.$e->getMessage().')</dd>';
					}
				}
				else
				{
					$result_list .= '<dd class="warning">'.$user_name.': no text message number available.</dd>';
				}
			}
			
			$result_list .= '</dl>';
			
			$this->alert('Message sent!'.$result_list, 'blue');
			$this->redirectFromHere(array('action' => 'recipients', 'recipients' => NULL, 'message' => NULL));
		}
		
		$this->view->users = $users;
		$this->view->recipients = implode(',', $recipients);
	}
}